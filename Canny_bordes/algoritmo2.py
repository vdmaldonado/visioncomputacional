
#Autor: Creado por Víctor Maldonado 
#Editado por: sin editas
#Mail: vdmaldonado@uce.edu.ec
#Estado: sin modificar

import numpy as np
import cv2

# Cargamos la imagen
original1 = cv2.imread("bolas_colores.png")
original = cv2.resize(original1, (400, 600))
cv2.imshow("original", original)

# Convertimos a escala de grises
gris = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)

# Aplicar suavizado Gaussiano
gauss1 = cv2.GaussianBlur(gris, (5, 5), 0)
gauss2 = cv2.GaussianBlur(gauss1, (5, 5), 0)
gauss3 = cv2.GaussianBlur(gauss2, (5, 5), 0)
gauss4 = cv2.GaussianBlur(gauss3, (5, 5), 0)
gauss = cv2.GaussianBlur(gauss4, (5, 5), 0)

cv2.imshow("suavizado", gauss)

# Detectamos los bordes con Canny
canny = cv2.Canny(gauss, 50, 150)

cv2.imshow("canny", canny)

# Buscamos los contornos
(contornos, _) = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Mostramos el número de monedas por consola
print("He encontrado {} objetos".format(len(contornos)))

cv2.drawContours(original, contornos, -1, (0, 0, 255), 2)
cv2.imshow("contornos", original)

cv2.waitKey(0)