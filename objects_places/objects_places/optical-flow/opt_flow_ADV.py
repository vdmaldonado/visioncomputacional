#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 19:51:16 2019

@author: felipe
"""
#!/usr/bin/env python

#'''
#example to show optical flow
#
#USAGE: opt_flow.py [<video_source>]
#
#Keys:
# 1 - toggle HSV flow visualization
# 2 - toggle glitch
#
#Keys:
#    ESC    - exit
#'''
# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import math
import cv2
import video

def up(x1,y1,x2,y2):
    vectU = np.array([x2-x1,y2-y1])
    #print(vect)
    matrixU= np.matrix('1;0')
    #print(matriz)
    if np.linalg.norm(vectU) ==0:
        res=0
    else:
        res = np.dot(vectU,matrixU)/np.linalg.norm(vectU)        
    if res >0:
        #print('arriba')
        return round(float(res),4)
    else:
        return 0
def down(x1,y1,x2,y2):
    vectD = np.array([x2-x1,y2-y1])
    #print(vect)
    matrixD= np.matrix('0;-1')
    #print(matriz)
    if np.linalg.norm(vectD) ==0:
        res=0
    else:
        res = np.dot(vectD,matrixD)/np.linalg.norm(vectD)
    if res >0:
        #print('abajo')
        return round(float(res),4)
    else:
        return 0
def left(x1,y1,x2,y2):
    vectL = np.array([x2-x1,y2-y1])
    #print(vect)
    matrixL= np.matrix('-1;0')
    #print(matriz)
    if np.linalg.norm(vectL) ==0:
        res=0
    else:
        res = np.dot(vectL,matrixL)/np.linalg.norm(vectL)
        
    if res >0:
        #print('izquierda')
        return round(float(res),4)
    else:
        return 0
def rigth(x1,y1,x2,y2):
    vectR = np.array([x2-x1,y2-y1])
    #print(vect)
    matrixR= np.matrix('1;0')
    #print(matriz)
    if np.linalg.norm(vectR) ==0:
        res=0
    else:
        res = np.dot(vectR,matrixR)/np.linalg.norm(vectR)        
    if res >0:
        #print('derecha')
        return round(float(res),4)
    else:
        return 0
    

def draw_flow(img, flow, step=8):
    global arrows
    global adv
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    k=0
    for (x1, y1), (x2, y2) in lines:
        k=k+1
        #print("k=",k," x1 ",x1," y1 ",y1," x2 ",x2," y2 ",y2)
#        Up =up(x1,y1,x2,y2)
#        Down = down(x1,y1,x2,y2)
#        Left = left(x1,y1,x2,y2)
#        Right = rigth(x1,y1,x2,y2)
        #adv.append([Up,Down,Left,Right])
        arrows.append([x1,y1, math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))])
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    print("k=",k)    
    return vis
    


def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr


def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res

if __name__ == '__main__':
    import sys
    import cv2 as cv
    import time 
    print(__doc__)
    try:
        fn = sys.argv[1]
    except IndexError:
        fn = 0

    arrows = []
    adv = []
    #fn
    #'v_golf_02_02.mpg'
    cam = video.create_capture(fn)
    #cap = cv.VideoCapture("Hooligans_violence__ACHTUNG.avi")
    cap=cam
    length = int(cap.get(cv.CAP_PROP_FRAME_COUNT)) 
    print("Longutid: ",length) 
    width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
    print("width: ",width)
    height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)) 
    print("height: ",height)
    fps = cap.get(cv.CAP_PROP_FPS) 
    print("fps: ",fps)
    ret, prev = cam.read()
    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    show_hsv = False
    show_glitch = False
    cur_glitch = prev.copy()
    cont = 0
    while True:
        cont = cont+1
        if cont == 4:
            break
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray, gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)
        Lt = -1*flow[...,0]
        Rt = flow[...,0]
        Ut = -1*flow[...,1]
        Dt = flow[...,1]
        # Si cada uno de los valores de la matriz u(horizontal) de la matriz (x) son >0 reemplazar por cero
        u = flow[...,0]
        v = flow[...,1]
#    for i in range(0, len(u)):
#        print("coordenadas: ",i)
        for i in range(len(u)):
            for j in range(len(u[i])):
                if u[i][j] > 0:
                    Lt[i][j] = 0
                if u[i][j] < 0:
                    Rt[i][j] = 0
                if v[i][j] > 0:
                    Ut[i][j] = 0
                if v[i][j] < 0:
                    Dt[i][j] = 0
            #print(u[i][j], end=' ')
        #print()
        #Ltt(:,:,k)= Lt;
        
        if cont == 1:
            L = Lt
            R = Rt
            U = Ut
            D = Dt
            F = gray
        else:
#            np.dstack((Ltt, Lt))
#            np.dstack((Rtt, Rt))
#            np.dstack((Utt, Ut))
#            np.dstack((Dtt, Dt))
            L = L+Lt
            R = R+Rt
            U = U+Ut
            D = D+Dt
            F = F+gray
        
        Lt2 = cv.normalize(Lt,None,0,255,cv.NORM_MINMAX)
    #tempLt=cv.cvtColor(Lt2,cv.COLOR_HSV2BGR)
        cv.imshow('frameLT',Lt2)
        prevgray = gray

        arrows.clear()
        adv.clear()
        finalImg = draw_flow(gray,flow)
        #print(arrows)
        #print('Arrows: ',arrows)
        #print('ADV: ',adv)
        print("# ", cont)
             # if cont 
        cv2.imshow('flow', finalImg)
        if show_hsv:
            cv2.imshow('flow HSV', draw_hsv(flow))
        if show_glitch:
            cur_glitch = warp_flow(cur_glitch, flow)
            cv2.imshow('glitch', cur_glitch)

        ch = cv2.waitKey(5)
        if ch == 27:
            break
        if ch == ord('1'):
            show_hsv = not show_hsv
            print('HSV flow visualization is', ['off', 'on'][show_hsv])
        if ch == ord('2'):
            show_glitch = not show_glitch
            if show_glitch:
                cur_glitch = img.copy()
            print('glitch is', ['off', 'on'][show_glitch])
 
    print("tamaño Lt",L.shape)
    print(L)
    F = cv2.normalize(F,None,0,255,cv2.NORM_MINMAX)
    cv2.imshow('IMAGEN',F)
    #k = cv2.waitKey(30) & 0xff
    time.sleep(10)
    #cv2.destroyAllWindows()
    